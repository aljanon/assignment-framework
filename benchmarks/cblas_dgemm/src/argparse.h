#ifndef ARGPARSE_H
#define ARGPARSE_H 1

#include <stdlib.h>

struct arguments {
	int size;
	int K, M, N;
	double alpha, beta;
	short quiet, verbose;
	unsigned int seed;
};

int argparse(int argc, char **argv, struct arguments *arguments);

#endif