#include "log.h"
#include <stdio.h>
#include <stdarg.h>
#include <time.h>
#include <stdint.h>
#include <unistd.h>

uint8_t runtime_log_level = COMPILE_LOG_LEVEL;

const char *level_names[] = {
	"TRACE", "DEBUG", "INFO", "WARNING", "ERROR", "FATAL"
};

const char *level_colors[] = {
	COLOR_DEFAULT, COLOR_CYAN, COLOR_BLUE,
	COLOR_YELLOW, COLOR_RED, COLOR_MAGENTA
};

const int level_fds[] = {
	STDOUT_FILENO, STDOUT_FILENO, STDOUT_FILENO,
	STDOUT_FILENO, STDERR_FILENO, STDERR_FILENO
};

void log_msg(enum log_level level, const char *fmt, ...)
{
	if (level < runtime_log_level)
		return;
	time_t t = time(NULL);
	struct tm *lt = localtime(&t);
	va_list args;
	char buf[32];
	buf[strftime(buf, sizeof(buf), "%Y-%m-%d %H:%M:%S", lt)] = '\0';
	dprintf(level_fds[level], "[%s] %s%-7s" RESET_ATTR ": ",
		buf, level_colors[level], level_names[level]);
	va_start(args, fmt);
	vdprintf(level_fds[level], fmt, args);
	va_end(args);
	dprintf(level_fds[level], "\n");
}