#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <errno.h>
#include <string.h>
#include <cblas.h>
#include <locale.h>
#include <time.h>
#include "log.h"
#include "argparse.h"

#define err_exit(msg) do { perror(msg); exit(EXIT_FAILURE); } while(0)
#define init_array(array, size) do{ \
	for(typeof(size) __i = 0; __i < (size); __i++) { \
		(array)[__i] = random(); \
	} \
} while (0)

void update_log_level(struct arguments *arguments)
{
	short log_change = arguments->quiet - arguments->verbose;
	short new_log_level = runtime_log_level + log_change;
	if(new_log_level >= LOG_FATAL) {
		runtime_log_level = LOG_FATAL;
	} else if(new_log_level < LOG_TRACE) {
		runtime_log_level = LOG_TRACE;
	} else {
		runtime_log_level = new_log_level;
	}
}

int main(int argc, char **argv)
{
	struct arguments arguments = {.alpha = 1.0};
	argparse(argc, argv, &arguments);
	update_log_level(&arguments);
	setlocale(LC_NUMERIC, "");
	arguments.K = arguments.M = arguments.N = arguments.size;

	log_msg(LOG_INFO, "alpha = %f", arguments.alpha);
	log_msg(LOG_INFO, "beta = %f", arguments.beta);

	int size_A = arguments.M * arguments.K;
	int size_B = arguments.K * arguments.N;
	int size_C = arguments.M * arguments.N;

	log_msg(LOG_INFO, "Allocating A of size %'d (%'d x %'d)",
		size_A, arguments.M, arguments.K);
	double *A = calloc(size_A, sizeof(double));
	log_msg(LOG_INFO, "Allocating B of size %'d (%'d x %'d)",
		size_B, arguments.K, arguments.N);
	double *B = calloc(size_B, sizeof(double));
	log_msg(LOG_INFO, "Allocating C of size %'d (%'d x %'d)",
		size_C, arguments.M, arguments.N);
	double *C = calloc(size_C, sizeof(double));

	if(!A || !B || !C) {
		log_msg(LOG_FATAL, "Could not allocate memory: %s. Aborting.",
			strerror(errno));
		exit(EXIT_FAILURE);
	}

	log_msg(LOG_INFO, "Setting seed %d.", arguments.seed);
	srandom(arguments.seed);

	log_msg(LOG_INFO, "Initializing A randomly.");
	init_array(A, size_A);
	log_msg(LOG_INFO, "Initializing B randomly.");
	init_array(B, size_B);
	log_msg(LOG_INFO, "Initializing C randomly.");
	init_array(C, size_C);

	log_msg(LOG_INFO,
		"Computing C = alpha*A*B + beta*C using cblas_dgemm.");

	struct timespec debut, fin;
	clock_gettime(CLOCK_REALTIME, &debut);
	cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans,
		    arguments.M, arguments.N, arguments.K, 
		    arguments.alpha, A, arguments.K, B, arguments.N,
		    arguments.beta, C, arguments.N);
	clock_gettime(CLOCK_REALTIME, &fin);
	uint64_t temps_nano = (fin.tv_nsec - debut.tv_nsec)
				   + 1e9 * (fin.tv_sec - debut.tv_sec);
	fprintf(stderr, "%ld\n", temps_nano);

	free(A);
	free(B);
	free(C);
	exit(EXIT_SUCCESS);
}