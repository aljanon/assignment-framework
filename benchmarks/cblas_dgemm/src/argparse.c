#include <argp.h>
#include "argparse.h"

#define OPT_SEED 1

const char *argp_program_version =
	"cblas_dgemm 0.1";

/* Program documentation. */
static char doc[] = "Computes C = alpha*A*B + beta*C." \
/* "Matrices sizes:" \
"A: M*K" \
"B: K*N" \
"C: M*N" \
"If no specific values are provided, the program expects one unnamed argument" \
"for the size. In that case, K=M=N=SIZE" \ */
" A, B, and C are initialized randomly using the random() function";

/* A description of the arguments we accept. */
static char args_doc[] = "SIZE";

/* The options we understand. */
static struct argp_option options[] = {
	// {0,         'K', "SIZE",  0, "Integer", 0},
	// {0,         'M', "SIZE",  0, "Integer", 0},
	// {0,         'N', "SIZE",  0, "Integer", 0},
	{"alpha",   'a', "VALUE", 0, "Double value (default 1.0)", 0},
	{"beta",    'b', "VALUE", 0, "Double value (default 0.0)", 0},
	{"seed",     OPT_SEED, "SEED",  0, "Seed (default 0)", 0},
	{"verbose", 'v', 0,       0, "Increase verbosity", 0},
	{"quiet",   'q', 0,       0, "Decrease verbosity", 0},
	{0}
};

/* Parse a single option. */
static error_t parse_opt(int key, char *arg, struct argp_state *state)
{
	/* Get the input argument from argp_parse, which we
   	know is a pointer to our arguments structure. */
	struct arguments *arguments = state->input;

	switch(key) {
	case 'K':
		arguments->K = atoi(arg);
		break;
	case 'M':
		arguments->M = atoi(arg);
		break;
	case 'N':
		arguments->N = atoi(arg);
		break;
	case 'a':
		arguments->alpha = atoi(arg);
		break;
	case 'b':
		arguments->beta = atoi(arg);
		break;
	case OPT_SEED:
		arguments->seed = atoi(arg);
		break;
  	case 'q':
    		arguments->quiet += 1;
    		break;
  	case 'v':
    		arguments->verbose += 1;
    		break;
  	case ARGP_KEY_NO_ARGS:
  		argp_usage(state);
		break;
  	case ARGP_KEY_ARG:
		/* Here we know that state->arg_num == 0, since we
		force argument parsing to end before any more arguments can
		get here. */
		arguments->size = atoi(arg);

		/* In addition, by setting state->next to the end
		of the arguments, we can force argp to stop parsing here and
		return. */
		state->next = state->argc;

		break;
  	default:
    		return ARGP_ERR_UNKNOWN;
  	}
	return 0;
}

/* Our argp parser. */
static struct argp argp = {options, parse_opt, args_doc, doc, 0, 0, 0};

int argparse(int argc, char **argv, struct arguments *arguments)
{
	return argp_parse(&argp, argc, argv, 0, 0, arguments);
}