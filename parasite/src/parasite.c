#define _GNU_SOURCE
#ifndef LEVEL1_DCACHE_LINESIZE
#define LEVEL1_DCACHE_LINESIZE 64
#endif

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <locale.h>
#include <time.h>
#include <gsl/gsl_randist.h>
#include "argparse.h"
#include "log.h"

#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)

#define err_exit(msg) do{ perror(msg); exit(EXIT_FAILURE); } while(0)

struct elem {
	int v;
	struct elem *n;
} __attribute__((aligned(LEVEL1_DCACHE_LINESIZE)));

void random_init_array(struct elem *array, size_t size, unsigned long int seed)
{
	/* cf. Swann Perarnau's PhD thesis p.23, fig. 2.7 */
	/* randomisation de la liste
	 * mt19937 est un générateur de qualité (Mersenne twister)
	 */
	size_t i;
	gsl_rng *r = gsl_rng_alloc(gsl_rng_mt19937);
	gsl_rng_set(r, seed);
	for(i = 0; i < size; i++) {
		array[i].v = i;
	}
	gsl_ran_shuffle(r, (void*) array, size, sizeof(struct elem));
	struct elem *cur = &(array[array[0].v]);
	for(i= 0; i < size; i++) {
		cur->n = &(array[array[i].v]);
		cur = cur->n;
	}
	cur->n = &(array[array[0].v]);
	gsl_rng_free(r);
}

void access_loop(struct elem *array, size_t max)
{
	/* cf. Swann Perarnau's PhD thesis p.23, fig. 2.7 */
	size_t i;
	struct elem *cur = &(array[array[0].v]);
	/* volatile évite que gcc considère la boucle et la somme
	 * comme inutile */
	volatile int somme = 0;
	for(i = 0; i < max; ++i) {
		somme += cur->v;
		cur = cur->n;
	}
}

void cache_access_times(struct elem *array, size_t max)
{
	/* cf. Swann Perarnau's PhD thesis p.23, fig. 2.7 */
	struct timespec debut, fin;
	clock_gettime(CLOCK_REALTIME, &debut);
	access_loop(array, max);
	clock_gettime(CLOCK_REALTIME, &fin);
	uint64_t temps_nano = (fin.tv_nsec - debut.tv_nsec)
				   + 1e9 * (fin.tv_sec - debut.tv_sec);
	fprintf(stderr, "%ld\n", temps_nano);
}

int main(int argc, char **argv)
{
	struct arguments arguments = {0};
	struct elem *array;
	argparse(argc, argv, &arguments);
	argcheck(&arguments);
	setlocale(LC_NUMERIC, "");
	log_msg(LOG_INFO, "Seed: %d", arguments.seed);
	log_msg(LOG_INFO, "Loop count: %'d", arguments.loop_count);
	log_msg(LOG_INFO, "Allocating %'d elements of size %'d",
		arguments.nmemb, sizeof(typeof(*array)));
	if(!(array = calloc(arguments.nmemb, sizeof(typeof(*array)))))
		err_exit("calloc");
	log_msg(LOG_INFO, "Allocated %'d B",
		arguments.nmemb * sizeof(typeof(*array)));
	random_init_array(array, arguments.nmemb, arguments.seed);
	if(arguments.single) {
		cache_access_times(array, arguments.loop_count);
	} else {
		if(!arguments.continuous)
			log_msg(LOG_INFO, "Sleep time: %'d us",
				arguments.usecs);
		while(1) {
			access_loop(array, arguments.loop_count);
			if(!arguments.continuous)
				usleep(arguments.usecs);
		}
	}
	free(array);
	exit(EXIT_SUCCESS);
}