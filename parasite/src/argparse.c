#include <math.h>
#include <argp.h>
#include "argparse.h"
#include "log.h"

#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)

#define CACHE_INDEX_DEFAULT 3
#define SLEEP_MIN 10
#define ACCESS_COUNT_DEFAULT 4

#define OPT_SEED 1
#define OPT_CONTINUOUS 2
#define OPT_SINGLE 3

const char *argp_program_version =
	"parasite 0.1";

/* Program documentation. */
static char doc[] = "TODO";

/* A description of the arguments we accept. */
static char args_doc[] = "SIZE";

/* The options we understand. */
static struct argp_option options[] = {
	{"cache",    'c', "INDEX", 0,
		"Cache index (default " STR(CACHE_INDEX) ")", 0},
	{"nmemb",    'n', "SIZE",  0, "Bytes count (default 0)", 0},
	{"seed",     OPT_SEED, "SEED",  0, "Seed (default 0)", 0},
	{"sleep",    's', "USECS", 0,
		"Sleep time in us (default " STR(SLEEP_MIN)
		", min " STR(SLEEP_MIN) ")", 0},
	{"loops",    'l', "COUNT", 0, "Loop count (default nmemb)", 0},
	{"continuous", OPT_CONTINUOUS, 0, 0, "Continuous accesses", 0},
	{"single", OPT_SINGLE, 0, 0,
		"Single run of the access loop. "
		"Will print the execution of the given loop count to "
		"the standard error output."
		"If both --continuous and --single are given, "
		"--continuous will be ignored.", 0},
	{"access",   'a', "ACCESS_COUNT", 0,
		"Single mode only. (default "STR(ACCESS_COUNT_DEFAULT) "). "
		"Average access count for each element. "
		"Takes precedence over the loop count parameter "
		"as it will compute the loop count as: "
		"ACCESS_COUNT * SIZE * log2(SIZE) ", 0},
	{"verbose",  'v', 0,       0, "Increase verbosity", 0},
	{"quiet",    'q', 0,       0, "Decrease verbosity", 0},
	{0}
};

/* Parse a single option. */
static error_t parse_opt(int key, char *arg, struct argp_state *state)
{
	/* Get the input argument from argp_parse, which we
   	know is a pointer to our arguments structure. */
	struct arguments *arguments = state->input;

	switch(key) {
	case 'c':
		arguments->cache_index = atoi(arg);
		break;
	case 'n':
		arguments->nmemb = atoi(arg);
		break;
	case OPT_SEED:
		arguments->seed = atoi(arg);
		break;
	case 's':
		arguments->usecs = atoi(arg);
		break;
	case 'l':
		arguments->loop_count = atoi(arg);
		break;
	case OPT_CONTINUOUS:
		arguments->continuous = 1;
		break;
	case OPT_SINGLE:
		arguments->single = 1;
		break;
	case 'a':
		arguments->access_count = atoi(arg);
		break;
  	case 'q':
    		arguments->quiet += 1;
    		break;
  	case 'v':
    		arguments->verbose += 1;
    		break;
  	case ARGP_KEY_NO_ARGS:
		break;
  	case ARGP_KEY_ARG:
		/* Here we know that state->arg_num == 0, since we
		force argument parsing to end before any more arguments can
		get here. */
		arguments->nmemb = atoi(arg);

		/* In addition, by setting state->next to the end
		of the arguments, we can force argp to stop parsing here and
		return. */
		state->next = state->argc;

		break;
  	default:
    		return ARGP_ERR_UNKNOWN;
  	}
	return 0;
}

/* Our argp parser. */
static struct argp argp = {options, parse_opt, args_doc, doc, 0, 0, 0};

int argparse(int argc, char **argv, struct arguments *arguments)
{
	return argp_parse(&argp, argc, argv, 0, 0, arguments);
}

unsigned int loop_count(struct arguments *arguments)
{
	double res = arguments->nmemb * log2(arguments->nmemb) +
			(arguments->access_count - 1) * 
				log2(log2(arguments->nmemb));
	return res;
}

void argcheck(struct arguments *arguments)
{
	update_log_level(arguments->quiet - arguments->verbose);
	if(arguments->usecs < SLEEP_MIN)
		arguments->usecs = SLEEP_MIN;
	if(!arguments->loop_count) {
		arguments->loop_count = arguments->nmemb;
		if(arguments->single)
			arguments->loop_count = loop_count(arguments);
	}
}