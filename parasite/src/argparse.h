#ifndef ARGPARSE_H
#define ARGPARSE_H 1

#include <stdlib.h>

struct arguments {
	size_t nmemb;
	unsigned int loop_count;
	unsigned int usecs;
	unsigned int cache_index;
	unsigned int seed;
	int continuous;
	int single;
	unsigned int access_count;
	short quiet, verbose;
};

int argparse(int argc, char **argv, struct arguments *arguments);

void argcheck(struct arguments *arguments);

#endif