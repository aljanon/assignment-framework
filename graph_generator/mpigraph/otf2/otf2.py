import otf2
from otf2.events import *
import itertools
from mpigraph.graph import Graph
import subprocess
import os
import logging

logger = logging.getLogger("mg." + __name__)

FILTER_FILE = ".scorep-filter"
FILTER_CONTENTS = "SCOREP_REGION_NAMES_BEGIN EXCLUDE\n*\n"
TRACE_FILE = "traces.otf2"
MEASUREMENTS_DIR = "scorep-measurement"


def default_trace_path(output_dir):
    return os.path.join(output_dir, MEASUREMENTS_DIR, TRACE_FILE)


def read_trace(trace_path):
    G = Graph()
    with otf2.reader.open(trace_path) as trace:
        logger.info("Reading OTF2 trace")
        mpi_comm_world = trace.definitions.comms[0]
        count = 0
        total = len(trace.events)
        logger.info("Found {} events".format(total))
        for location, event in trace.events:
            count += 1
            logger.debug("Processing event {}/{}".format(count, total))
            if isinstance(event, (MpiSend, MpiIsend)):
                sender = mpi_comm_world.rank(location)
                receiver_location = event.communicator.location(event.receiver)
                receiver = mpi_comm_world.rank(receiver_location)
                G.add_edge(sender, receiver, info=event.msg_length,
                           info_key="mpi")
            elif isinstance(event, MpiCollectiveEnd):
                m = itertools.combinations(
                        map(lambda x: mpi_comm_world.rank(x),
                            event.communicator.group.members), 2)
                info = max(event.size_sent, event.size_received)
                for v1, v2 in m:
                    G.add_edge(v1, v2, info=info,
                               info_key="mpi")
    return G


def read_trace_metric(trace_path, node_rank):
    cache_events = {}
    with otf2.reader.open(trace_path) as trace:
        mpi_comm_world = trace.definitions.comms[0]
        for location, event in trace.events:
            if not isinstance(event, Metric):
                continue
            rank = mpi_comm_world.rank(location)
            if node_rank != rank:
                continue
            metrics = event.metric.members
            for i in range(len(metrics)):
                if metrics[i].name not in cache_events:
                    cache_events[metrics[i].name] = 0
                cache_events[metrics[i].name] += event.values[i]
    return cache_events["cache-misses"] / cache_events["cache-references"]


def prepare_filter(filter_path):
    with open(filter_path, "w") as file:
        file.write(FILTER_CONTENTS)


def prepare_env(output_dir):
    # copy of current env: inherit vars, avoid modifiying current exec
    env = os.environ.copy()
    filter_path = os.path.join(output_dir, FILTER_FILE)
    prepare_filter(filter_path)
    # redefine some environment variables (as some may already by set)
    # most are reset back to their default
    env["SCOREP_ENABLE_PROFILING"] = "false"
    env["SCOREP_ENABLE_TRACING"] = "true"
    env["SCOREP_ENABLE_UNWINDING"] = "false"
    env["SCOREP_VERBOSE"] = "false"
    env["SCOREP_EXPERIMENT_DIRECTORY"] = os.path.join(output_dir,
                                                      MEASUREMENTS_DIR)
    env["SCOREP_OVERWRITE_EXPERIMENT_DIRECTORY"] = "true"
    env["SCOREP_MACHINE_NAME"] = subprocess.run("hostname",
                                                stdout=subprocess.PIPE).stdout
    env["SCOREP_FILTERING_FILE"] = filter_path
    env["SCOREP_MPI_MAX_COMMUNICATORS"] = "50"
    env["SCOREP_MPI_MAX_WINDOWS"] = "50"
    env["SCOREP_MPI_MAX_ACCESS_EPOCHS"] = "50"
    env["SCOREP_MPI_MAX_GROUPS"] = "50"
    env["SCOREP_MPI_ENABLE_GROUPS"] = "cg,coll,p2p"
    env["SCOREP_MPI_MEMORY_RECORDING"] = "false"
    env["SCOREP_MPI_ONLINE_ANALYSIS"] = "false"
    env["SCOREP_TIMER"] = "clock_gettime"
    env["SCOREP_METRIC_PERF"] = "cache-misses,cache-references"
    env["SCOREP_METRIC_PERF_SEP"] = ","
    env["SCOREP_TOTAL_MEMORY"] = "64M"
    return env


def get_mpi_graph(mpi_cmd, output_dir):
    logger.info("Computing MPI communication graph")
    mpi = subprocess.Popen(mpi_cmd, env=prepare_env(output_dir), shell=True)
    mpi.wait()
    if mpi.returncode != 0:
        raise subprocess.CalledProcessError(mpi.returncode, mpi.args)
    return read_trace(default_trace_path(output_dir))
