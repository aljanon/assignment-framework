def write_mapping(mapping, path):
    with open(path, 'w') as rankfile:
        for (rank, pu) in sorted(mapping.items()):
            rankfile.write("rank {}=localhost slot={}\n".format(rank, pu))


def write_cache_exp_rankfile(G, target, topo, path):
    mapping = {}
    for node in G:
        mapping[node] = node
    i0 = next(iter(topo.graph))
    i1 = topo.next_obj_sharing_cache(i0).logical_index
    mapping[target] = i0
    # print("i0: {}".format(i0))
    # print("i1: {}".format(i1))
    # print("target: {}".format(target))
    # print(mapping)
    if i0 in mapping:
        mapping[i0] = target
    if i1 in mapping:
        mapping[i1] = target
    # print(mapping)
    write_mapping(mapping, path)
    return (i0, i1)
