import pandas as pd
import subprocess
import shlex
import os
import logging

logger = logging.getLogger("mg." + __name__)

CMT_FILE = "cmt.csv"
PQOS_INTERVAL = 1


def cache_usage(mpi_cmd, obj_list, output_dir):
    pqos_path = os.path.join(output_dir, CMT_FILE)
    pqos_cmd_str = ("pqos -r -i {} -u csv -m 'llc:{}' -o '{}'"
                    .format(PQOS_INTERVAL, obj_list, pqos_path))
    logger.info("Computing cache usage using Intel CMT")
    pqos = subprocess.Popen(shlex.split(pqos_cmd_str),
                            stdout=subprocess.DEVNULL,
                            stderr=subprocess.DEVNULL)
    mpi = subprocess.run(mpi_cmd, shell=True,
                         stdout=subprocess.DEVNULL,
                         stderr=subprocess.DEVNULL)
    pqos.terminate()
    mpi.check_returncode()
    df = pd.read_csv(pqos_path)
    usageKB = df.groupby("Core").mean().drop(columns=["IPC", "LLC Misses"])
    usageB = usageKB.apply(lambda x: x*1024)
    return usageB.to_dict()["LLC[KB]"]
