from mpigraph.mpiconfig import write_cache_exp_rankfile
from mpigraph.otf2 import default_trace_path, read_trace_metric, prepare_env
import subprocess
import shlex
import os
import logging

logger = logging.getLogger("mg." + __name__)

POINTS = 3
SEED_COUNT = 1
DF_COLUMNS = ["rank", "para_size", "cache_misses"]
HOSTFILE = "hostfile"


def rank_cache_interference(rank, topo, pu0, pu1, mpi_cmd, parasite_cmd,
                            output_dir, cache_interference_dict):
    blc, llc = topo.exp_cache_linecount(pu0, pu1)
    linesize = topo.exp_cache_linesize(pu0, pu1)
    trace_path = default_trace_path(output_dir)
    rankfile_path = os.path.join(output_dir, "rankfile_" + str(rank))
    hostfile_path = os.path.join(output_dir, HOSTFILE)
    cache_interference_dict[rank] = {}
    mpi = subprocess.Popen(mpi_cmd,
                           env=prepare_env(output_dir), shell=True,
                           stdout=subprocess.DEVNULL,
                           stderr=subprocess.DEVNULL)
    mpi.wait()
    if mpi.returncode != 0:
        raise subprocess.CalledProcessError(mpi.returncode,
                                            " ".join(mpi.args))
    cache_misses = read_trace_metric(trace_path, rank)
    cache_interference_dict[rank][0] = cache_misses / SEED_COUNT
    for para_size in range(blc, llc, (llc - blc) // POINTS):
        cache_misses = 0
        logger.info("Computing cache interference for rank {}: size = {}"
                    .format(rank, para_size * linesize))
        for seed in range(SEED_COUNT):
            para_full_str = ("hwloc-bind {}:{} {} --continuous -n {} --seed={}"
                             .format(topo.type_string_from_index(pu1), pu1,
                                     parasite_cmd, para_size, seed))
            para_full = shlex.split(para_full_str)
            mpi_cmd_rankfile = shlex.split(mpi_cmd)
            mpi_cmd_rankfile.insert(1, "-rf")
            mpi_cmd_rankfile.insert(2, rankfile_path)
            mpi_cmd_rankfile.insert(3, "-hostfile")
            mpi_cmd_rankfile.insert(4, hostfile_path)
            mpi_cmd_full = " ".join(mpi_cmd_rankfile)
            parasite = subprocess.Popen(para_full, stdout=subprocess.DEVNULL,
                                        stderr=subprocess.DEVNULL)
            mpi = subprocess.Popen(mpi_cmd_full,
                                   env=prepare_env(output_dir), shell=True,
                                   stdout=subprocess.DEVNULL,
                                   stderr=subprocess.DEVNULL)
            mpi.wait()
            parasite.terminate()
            if mpi.returncode != 0:
                raise subprocess.CalledProcessError(mpi.returncode, mpi.args)
            cache_misses += read_trace_metric(trace_path, rank)
        cache_interference_dict[rank][para_size * linesize] = \
            cache_misses / SEED_COUNT


def cache_interference(G, topo, mpi_cmd, parasite_cmd, output_dir):
    cache_interference_dict = {}
    with open(os.path.join(output_dir, HOSTFILE), "w") as file:
        file.write("localhost slots={}\n".format(G.number_of_nodes()))
    for rank in sorted(G):
        pu0, pu1 = write_cache_exp_rankfile(G, rank, topo,
                    os.path.join(output_dir, "rankfile_" + str(rank)))
        logger.info("Computing cache interference for rank {}/{}"
                    .format(rank, G.number_of_nodes() - 1))
        rank_cache_interference(rank, topo, pu0, pu1, mpi_cmd, parasite_cmd,
                                output_dir, cache_interference_dict)
    # Todo: write to csv
    return cache_interference_dict


def estimate_cache_interference(cache_usage_rank, cache_interference_rank):
    sizes = sorted(cache_interference_rank.keys())
    if cache_interference_rank.get(cache_usage_rank) is None:
        i = 0
        while i < len(sizes) - 1 and \
                (sizes[i] > cache_usage_rank or sizes[i+1] < cache_usage_rank):
            i += 1
        if i < len(cache_interference_rank) - 1:
            estimate = (cache_interference_rank[sizes[i]] +
                        cache_interference_rank[sizes[i+1]]) / 2
        else:
            estimate = cache_interference_rank[sizes[i]]
    else:
        estimate = cache_interference_rank[cache_usage_rank]
    estimate /= cache_interference_rank[sizes[0]]
    if estimate < 2:
        return 1
    return estimate


def add_cache_interference(Gmpi, cache_usage, cache_interference):
    logger.info("Adding cache interference to MPI graph")
    count = 0
    for (x, y) in Gmpi.edges:
        count += 1
        logger.info("Adding cache interference to edge ({}, {}). {}/{}"
                    .format(x, y, count, Gmpi.number_of_edges()))
        interference_x = \
            estimate_cache_interference(cache_usage[y], cache_interference[x])
        interference_y = \
            estimate_cache_interference(cache_usage[x], cache_interference[y])
        if "info" not in Gmpi[x][y]:
            Gmpi[x][y]["info"] = {}
        Gmpi[x][y]["info"]["interference"] = max(interference_x, interference_y)
    return Gmpi


def cache_misses_rankfile(G, mpi_cmd, output_dir, repeat_count, rankfile=None):
    rankfile_path = os.path.join(output_dir, "rankfile")
    hostfile_path = os.path.join(output_dir, HOSTFILE)
    mpi_cmd_rankfile = shlex.split(mpi_cmd)
    if rankfile is not None:
        mpi_cmd_rankfile.insert(1, "-rf")
        mpi_cmd_rankfile.insert(2, rankfile_path)
        mpi_cmd_rankfile.insert(3, "-hostfile")
        mpi_cmd_rankfile.insert(4, hostfile_path)
    mpi_cmd_full = " ".join(mpi_cmd_rankfile)
    cache_misses = {}
    for rank in G:
        cache_misses[rank] = 0
    for i in range(repeat_count):
        mpi = subprocess.Popen(mpi_cmd_full,
                               env=prepare_env(output_dir), shell=True,
                               stdout=subprocess.DEVNULL,
                               stderr=subprocess.DEVNULL)
        mpi.wait()
        if mpi.returncode != 0:
            raise subprocess.CalledProcessError(mpi.returncode, mpi.args)
        for rank in G:
            cache_misses[rank] += \
                read_trace_metric(default_trace_path(output_dir), rank)
    for rank in G:
        cache_misses[rank] /= repeat_count
    return cache_misses
