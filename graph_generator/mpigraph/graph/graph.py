import networkx as nx
import matplotlib.pyplot as plt


class Graph(nx.Graph):
    def add_edge(self, v1, v2, info=None, info_key=None):
        if info_key is not None and info is not None:
            info_dict = {}
            if ((v1, v2) in self.edges() and 'info' in self[v1][v2] and
               self[v1][v2]['info'] is not None):
                info_dict = self[v1][v2]['info']
            if info_key not in info_dict:
                info_dict[info_key] = [0]
            info_dict[info_key][0] += info
            super().add_edge(v1, v2, info=info_dict)
        else:
            super().add_edge(v1, v2)


def plot(G):
    plt.subplot(121)
    pos = nx.nx_pydot.graphviz_layout(G)
    nx.draw(G, pos=pos, with_labels=False)
    edge_labels = nx.get_edge_attributes(G, 'weight')
    nx.draw_networkx_edge_labels(G, pos=pos, edge_labels=edge_labels)
    node_labels = {}
    for i in G.nodes:
        node_labels[i] = i
        if 'weight' in G.nodes[i]:
            node_labels[i] = "{}\n{{{}}}".format(i, max(G.nodes[i]['weight']))
    nx.draw_networkx_labels(G, pos=pos, labels=node_labels)
    plt.show()


def write_dot(G, path):
    for k, v in nx.get_edge_attributes(G, 'weight').items():
        G.edges[k]['label'] = v
    for i in G.nodes:
        if 'weight' in G.nodes[i]:
            G.nodes[i]['label'] = "{}\n{{{}}}".format(i, G.nodes[i]['weight'])
    nx.drawing.nx_pydot.write_dot(G, path)


def write_metis(G, path):
    with open(path, 'w') as metis_file:
        metis_file.write("{} {} {}\n"
                         .format(G.number_of_nodes(), G.number_of_edges(), 1))
        for x in G.nodes:
            for y in G[x]:
                metis_file.write("{} {} "
                                 .format(y + 1, int(G[x][y]["weight"])))
            metis_file.write("\n")
