def compute_weights(Gpre_part):
    for (x, y) in Gpre_part.edges:
        Gpre_part[x][y]["weight"] = \
            sum(Gpre_part[x][y]["info"]["mpi"]) \
            / Gpre_part[x][y]["info"]["interference"]
    return Gpre_part
