from .partitioning import compute_weights
from .starpart import partition
