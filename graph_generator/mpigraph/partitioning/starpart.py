import networkx as nx
import os
from mpigraph.graph import write_metis
import subprocess
import re
import logging

logger = logging.getLogger("mg." + __name__)

SCHEME_LIST = ["KGGGP/PART", "METIS/RPART", "METIS/KPART", "MTMETIS/PART",
               "PULP/PART", "SCOTCH/KPART", "SCOTCH/RB", "SCOTCH/DEFAULT"]
GRAPH_FILE = "part.graph"


def partition_scheme(G, nparts, starpart_cmd, scheme, graph_path):
    strategy = ("\"BASIC/LOAD[filename={}];{}[nparts={}];" +
                "BASIC/PRINT[var=part,dump=1]\"") \
               .format(graph_path, scheme, nparts)
    args = starpart_cmd + " -r " + strategy + " | tail -n 1"
    starpart = subprocess.run(args, shell=True, stdout=subprocess.PIPE)
    starpart.check_returncode()
    stdout = starpart.stdout.decode("utf-8").strip()
    Gpart = nx.Graph()
    # Gpart.add_nodes_from(range(nparts))
    ansi_escape = re.compile(r'\x1B\[[0-?]*[ -/]*[@-~]')
    result = ansi_escape.sub("", stdout).split()
    part_to_partition = {}
    for i in range(len(result)):
        node = int(result[i], base=16)
        Gpart.add_node(node)
        if "parts" not in Gpart.nodes[node]:
            Gpart.nodes[node]["parts"] = []
        Gpart.nodes[node]["parts"].append(i)
        part_to_partition[i] = node
    for (x, y) in G.edges:
        node_x = part_to_partition[x]
        node_y = part_to_partition[y]
        if node_x != node_y:
            if (node_x, node_y) not in Gpart.edges:
                Gpart.add_edge(node_x, node_y)
            if "weight" not in Gpart[node_x][node_y]:
                Gpart[node_x][node_y]["weight"] = 0
            Gpart[node_x][node_y]["weight"] += sum(G[x][y]["info"]["mpi"])
        else:
            if "weight" not in Gpart.nodes[node_x]:
                Gpart.nodes[node_x]["weight"] = 0
            Gpart.nodes[node_x]["weight"] += G[x][y]["weight"]
    return Gpart


def partition_score(G):
    score = 0
    for node in G:
        if "weight" in G.nodes[node]:
            score += G.nodes[node]["weight"]
    for (x, y) in G.edges:
        score += G[x][y]["weight"]
    return score


def partition(G, nparts, starpart_cmd, output_dir):
    graph_path = os.path.join(output_dir, GRAPH_FILE)
    write_metis(G, graph_path)
    best_partition = None
    best_score = None
    best_scheme = None
    count = 0
    for scheme in SCHEME_LIST:
        count += 1
        logger.info("Computing partition for scheme {} ({}/{})"
                    .format(scheme, count, len(SCHEME_LIST)))
        Gpart = partition_scheme(G, nparts, starpart_cmd, scheme, graph_path)
        new_score = partition_score(Gpart)
        logger.info("Score for scheme {}: {}".format(scheme, new_score))
        if Gpart.number_of_nodes() == nparts and\
                (best_score is None or best_score > new_score):
            best_score = new_score
            best_partition = Gpart
            best_scheme = scheme
    logger.info("Best score is {} for scheme {}"
                .format(best_score, best_scheme))
    return best_partition, best_score, best_scheme
