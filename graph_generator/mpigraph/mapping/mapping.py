import networkx as nx
from pyscipopt import Model, quicksum
import itertools


# based on https://github.com/SCIP-Interfaces/PySCIPOpt/blob/833cdb4b6fb2890039c110a1b60d16c44d4d61dd/examples/finished/transp.py
def mapping_mip_model(partition: nx.Graph, topo: nx.Graph):

    model = Model("mapping")

    # Create variables
    x = {}

    for i in partition:
        for j in topo:
            x[i, j] = model.addVar(vtype="B", name="x({}, {})".format(i, j))

    for (i1, i2) in partition.edges:
        for (j1, j2) in itertools.permutations(topo, 2):
            x[i1, j1, i2, j2] = model.addVar(
                vtype="B", name="x({}, {}, {}, {})".format(i1, j1, i2, j2))
            model.addCons(x[i1, j1, i2, j2] <= x[i1, j1],
                          name="c1 x({}, {}, {}, {})".format(i1, j1, i2, j2))
            model.addCons(x[i1, j1, i2, j2] <= x[i2, j2],
                          name="c2 x({}, {}, {}, {})".format(i1, j1, i2, j2))
            model.addCons(x[i1, j1, i2, j2] + 1 >= x[i1, j1] + x[i2, j2],
                          name="c3 x({}, {}, {}, {})".format(i1, j1, i2, j2))

    # A partition is only affected to a single node
    for i in partition:
        model.addCons(quicksum(x[i, j] for j in topo) == 1,
                      name="AssignedToSingleNode({})".format(i))

    # A node has only a single partition affected to it
    for j in topo:
        model.addCons(quicksum(x[i, j] for i in partition) == 1,
                      name="SinglePartAssignedTo({})".format(j))
    # Objective
    model.setObjective(quicksum(
            quicksum(x[i1, j1, i2, j2] *
                     partition[i1][i2]["weight"] * topo[j1][j2]["weight"]
                     for (j1, j2) in itertools.permutations(topo, 2))
            for (i1, i2) in partition.edges),
        "minimize")

    model.hideOutput()
    model.optimize()

    model.data = x
    return model


def compute_mapping(partition: nx.Graph, topo: nx.Graph):
    if topo.number_of_nodes() != partition.number_of_nodes():
        raise ValueError("Inconsistent number of nodes")
    model = mapping_mip_model(partition, topo)
    model.optimize()
    data = model.data
    print("TotalTime: {}\nSolvingTime: {}, ReadingTime: {}, PresolvingTime:{}"
          .format(model.getTotalTime(), model.getSolvingTime(),
                  model.getReadingTime(), model.getPresolvingTime()))
    print("Cost of the mapping: {}".format(model.getObjVal()))
    mapping = {}
    for x in data:
        if len(x) != 2:
            continue
        i, j = x
        if model.getVal(data[i, j]):
            mapping[i] = j
    return mapping


def compute_final_mapping(Gpart, topo, mapping):
    new_mapping = {}
    for node in Gpart:
        core_list = topo.core_list(node)
        for i in range(len(node["parts"])):
            mapping[node["parts"][i]] = core_list[i % len(core_list)]
    return new_mapping
