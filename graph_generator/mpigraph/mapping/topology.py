import hwloc
import networkx as nx
import itertools
from enum import IntEnum


class HwlocType(IntEnum):
    PU = hwloc.OBJ_PU
    CORE = hwloc.OBJ_CORE
    PACKAGE = hwloc.OBJ_PACAGE
    NUMANODE = hwloc.OBJ_NUMANODE


class Topology(hwloc.Topology):
    def __init__(self, path=None, hwloc_type=HwlocType.CORE):
        super().__init__()
        self.path = path
        self.hwloc_type = hwloc_type
        if path is not None:
            self.set_xml(path)
        self.set_flags(hwloc.TOPOLOGY_FLAG_WHOLE_SYSTEM)
        self.load()

    def compute_graph(self):
        G = nx.Graph()
        root = self.root_obj
        for (c1, c2) in \
                itertools.combinations(self.objs_by_type(self.hwloc_type), 2):
            v1, v2 = c1.logical_index, c2.logical_index
            G.add_node(v1)
            G.add_node(v2)
            ancestor = root.get_common_ancestor(c1, c2)
            if ancestor.type == hwloc.OBJ_MACHINE:
                ancestor1 = c1.get_ancestor_obj_by_depth(ancestor.depth + 1)
                ancestor2 = c2.get_ancestor_obj_by_depth(ancestor.depth + 1)
                if ancestor1.type == hwloc.OBJ_PACAGE:
                    latency = 1
                if ancestor1.type == hwloc.OBJ_NUMANODE:
                    latency = self.get_latency(ancestor1, ancestor2)
                G.add_edge(v1, v2, weight=(0, latency))
            elif (ancestor.type == hwloc.OBJ_NUMANODE or
                    ancestor.type == hwloc.OBJ_PACAGE):
                G.add_edge(v1, v2, weight=(0, 1))
            elif ancestor.type == hwloc.OBJ_CACHE:
                G.add_edge(v1, v2, weight=(ancestor.attr.cache.size, 0))
            elif ancestor.type == hwloc.OBJ_CORE:
                cache = \
                    self.get_first_largest_obj_inside_cpuset(ancestor.cpuset)
                G.add_edge(v1, v2, weight=(cache.attr.cache.size, 0))
        return G

    def compute_numa_graph(self):
        G = nx.Graph()
        for (c1, c2) in \
                itertools.combinations(self.objs_by_type(hwloc.OBJ_NUMANODE), 2):
            v1, v2 = c1.logical_index, c2.logical_index
            G.add_node(v1)
            G.add_node(v2)
            G.add_edge(v1, v2, weight=self.get_latency(c1, c2))
        return G

    @property
    def numa_graph(self):
        if not hasattr(self, "_numa_graph"):
            self._numa_graph = self.compute_numa_graph()
        return self._numa_graph

    @property
    def graph(self):
        if not hasattr(self, "_graph"):
            self._graph = self.compute_graph()
        return self._graph

    def get_obj_from_index(self, index):
        return self.get_obj_by_type(self.hwloc_type, index)

    def next_obj_sharing_cache(self, index):
        return self.get_obj_from_index(index).next_cousin

    def exp_cache_linecount(self, i0, i1):
        c0 = self.get_obj_from_index(i0)
        c1 = self.get_obj_from_index(i1)
        LLC = c0.get_common_ancestor_obj(c1)
        BLC = LLC.children[0]
        return (BLC.attr.cache.size // BLC.attr.cache.linesize,
                LLC.attr.cache.size // LLC.attr.cache.linesize)

    def exp_cache_linesize(self, i0, i1):
        c0 = self.get_obj_from_index(i0)
        c1 = self.get_obj_from_index(i1)
        LLC = c0.get_common_ancestor_obj(c1)
        return c0.get_common_ancestor_obj(c1).attr.cache.linesize

    def type_string_from_index(self, index):
        return self.get_obj_from_index(index).type_string

    def obj_list(self):
        return ",".join(map(lambda x: str(x.logical_index),
                        self.objs_by_type(self.hwloc_type)))

    def core_list(self, numa_index):
        numanode = self.get_obj_by_type(hwloc.OBJ_CORE, numa_index)
        return list(self.objs_inside_cpuset_by_type(numanode.complete_cpuset,
                                                    hwloc.OBJ_CORE))
