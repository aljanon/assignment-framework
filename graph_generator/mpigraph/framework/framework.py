from mpigraph.otf2 import get_mpi_graph
from mpigraph.mapping import Topology, compute_mapping, compute_final_mapping
from mpigraph.cache import cache_usage, cache_interference, add_cache_interference
from mpigraph.partitioning import partition, compute_weights


class Assignment:
    def __init__(self, mpi_cmd, parasite_cmd, output_dir, hwloc_type,
                 starpart_cmd):
        self.mpi_cmd = mpi_cmd
        self.parasite_cmd = parasite_cmd
        self.output_dir = output_dir
        self.starpart_cmd = starpart_cmd
        self.topo = Topology(None, hwloc_type)

    @property
    def Gmpi(self):
        if not hasattr(self, "_Gmpi"):
            self._Gmpi = get_mpi_graph(self.mpi_cmd, self.output_dir)
        return self._Gmpi

    @property
    def cache_usage(self):
        if not hasattr(self, "_cache_usage"):
            self._cache_usage = cache_usage(self.mpi_cmd, self.topo.obj_list(),
                                            self.output_dir)
        return self._cache_usage

    @property
    def cache_interference(self):
        if not hasattr(self, "_cache_interference"):
            self._cache_interference = \
                cache_interference(self.Gmpi, self.topo, self.mpi_cmd,
                                   self.parasite_cmd, self.output_dir)
        return self._cache_interference

    @property
    def Gpre_part(self):
        if not hasattr(self, "_Gpre_part"):
            self._Gpre_part = \
                add_cache_interference(self.Gmpi, self.cache_usage,
                                       self.cache_interference)
            self._Gpre_part = compute_weights(self.Gpre_part)
        return self._Gpre_part

    @property
    def Gpart(self):
        if not hasattr(self, "_Gpart"):
            self._Gpart, self.Gpart_score, self.Gpart_scheme = \
                partition(self.Gpre_part,
                          self.topo.numa_graph.number_of_nodes(),
                          self.starpart_cmd, self.output_dir)
        return self._Gpart

    @property
    def numa_mapping(self):
        if not hasattr(self, "_numa_mapping"):
            self._numa_mapping = \
                compute_mapping(self.Gpart, self.topo.numa_graph)
        return self._numa_mapping

    @property
    def mapping(self):
        if not hasattr(self, "_mapping"):
            self._mapping = \
                compute_final_mapping(self.Gpart, self.topo, self.numa_mapping)
        return self._mapping
