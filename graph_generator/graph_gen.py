#!/usr/bin/python3

import argparse
import mpigraph as mg
import os
import logging
import subprocess
import shlex

logger = logging.getLogger("mg")


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--cmd",
                        help="application command with arguments")
    parser.add_argument("--parasite",
                        help="parasite command with arguments")
    parser.add_argument("--starpart-run",
                        help="starpart-run path")
    parser.add_argument("--topo-type",
                        choices=["CORE", "PACKAGE", "NUMANODE"],
                        default="CORE",
                        help="hwloc type for the topology")
    parser.add_argument("--output-dir",
                        default="./",
                        help="directory for storing results")
    return parser.parse_args()


def export_graph(G, filename, args):
    mg.graph.write_dot(G, os.path.join(args.output_dir, filename+".dot"))


def export_dict(d, filename, header, args):
    with open(os.path.join(args.output_dir, filename), "w") as file:
        file.write(header + "\n")
        for key in d:
            file.write("{},{}\n".format(key, d[key]))


def export_two_level_dict(d, filename, header, args):
    with open(os.path.join(args.output_dir, filename), "w") as file:
        file.write(header + "\n")
        for key in d:
            for nested_key in d[key]:
                file.write("{},{},{}\n"
                           .format(key, nested_key, d[key][nested_key]))


def execution_time(args, repeat_count, rankfile=None):
    rankfile_path = os.path.join(args.output_dir, "rankfile")
    hostfile_path = os.path.join(args.output_dir, "hostfile")
    mpi_cmd_rankfile = shlex.split(args.cmd)
    mpi_cmd_rankfile.insert(0, "/usr/bin/time")
    mpi_cmd_rankfile.insert(1, "-f")
    mpi_cmd_rankfile.insert(0, "\"%e,%U\"")
    if rankfile is not None:
        mpi_cmd_rankfile.insert(1, "-rf")
        mpi_cmd_rankfile.insert(2, rankfile_path)
        mpi_cmd_rankfile.insert(3, "-hostfile")
        mpi_cmd_rankfile.insert(4, hostfile_path)
    mpi_cmd_full = " ".join(mpi_cmd_rankfile)
    real_time = 0
    user_time = 0
    for i in range(repeat_count):
        mpi = subprocess.Popen(mpi_cmd_full,
                               env=mg.otf2.prepare_env(args.output_dir),
                               shell=True,
                               stdout=subprocess.DEVNULL,
                               stderr=subprocess.PIPE)
        mpi.wait()
        if mpi.returncode != 0:
            raise subprocess.CalledProcessError(mpi.returncode, mpi.args)
        time = mpi.stderr.decode("utf-8").split("\n")[-2].split(",")
        real_time += float(time[0])
        user_time += float(time[1])
    real_time /= repeat_count
    user_time /= repeat_count
    return real_time, user_time


def main():
    logging.basicConfig(format='[%(asctime)s] %(levelname)s: %(message)s',
                        level=logging.INFO)
    args = parse_args()
    if args.output_dir and not os.path.isdir(args.output_dir):
        logger.critical("{} is not a folder. Aborting."
                        .format(args.output_dir))
        exit(1)
    args.output_dir = os.path.abspath(args.output_dir)
    assignment = mg.framework.Assignment(args.cmd, args.parasite,
                                         args.output_dir,
                                         mg.mapping.HwlocType[args.topo_type],
                                         args.starpart_run)
    export_graph(assignment.Gmpi, "mpi", args)
    export_graph(assignment.topo.graph, "topology", args)
    export_graph(assignment.topo.numa_graph, "numa_topology", args)
    export_dict(assignment.cache_usage, "cache_usage", "rank,usage", args)
    export_two_level_dict(assignment.cache_interference, "cache_interference",
                          "rank,size,miss", args)
    # print(assignment.cache_usage)
    # print(assignment.cache_interference)
    export_graph(assignment.Gpre_part, "pre_part", args)
    export_graph(assignment.Gpart, "part", args)
    mg.mpiconfig.write_mapping(assignment.numa_mapping,
                               os.path.join(args.output_dir, "numa_rankfile"))
    mg.mpiconfig.write_mapping(assignment.numa_mapping,
                               os.path.join(args.output_dir, "rankfile"))
    old_misses = mg.cache.cache_misses_rankfile(assignment.Gmpi, args.cmd,
                                                args.output_dir, 1)
    new_misses = \
        mg.cache.cache_misses_rankfile(assignment.Gmpi, args.cmd,
                              args.output_dir, 1,
                              os.path.join(args.output_dir, "rankfile"))
    export_dict(old_misses, "old_misses", "rank,usage", args)
    export_dict(new_misses, "new_misses", "rank,usage", args)
    # print(old_misses)
    # print(new_misses)
    old_mean = sum(old_misses.values()) / len(old_misses)
    new_mean = sum(new_misses.values()) / len(new_misses)
    print("old cache mean: {}\n new cache mean: {}".format(old_mean, new_mean))
    old_real, old_user = execution_time(args, 1)
    new_real, new_user = \
        execution_time(args, 1, os.path.join(args.output_dir, "rankfile"))
    print("old_real: {}\nold_user: {}\nnew_real: {}, new_user: {}"
          .format(old_real, old_user, new_real, new_user))

if __name__ == "__main__":
    main()
