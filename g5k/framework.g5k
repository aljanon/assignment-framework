#!/bin/bash

########################################
# Critical functions
########################################

log_msg() {
	fmt="$1"
	shift
	script_name="$(basename ${BASH_SOURCE[${#BASH_SOURCE[@]} - 1]})"
	printf "[%s] {%s}: $fmt\n" "$(date --iso-8601=seconds)" "$script_name" "$@"
}

########################################
# Constants
########################################

version=0.1
script_name="$(basename ${BASH_SOURCE[${#BASH_SOURCE[@]} - 1]})"

########################################
# Script
########################################

software_dir="$(realpath $1)"
shift
[[ ! -d "$software_dir" ]] && log_msg "$software_dir is not a directory. Aborting." >&2 && exit 1
g5k_dir="$software_dir/g5k"
sys_info="$g5k_dir/framework_sys_info.g5k"
cblas_dgemm_dir="$software_dir/benchmarks/cblas_dgemm"
parasite_dir="$software_dir/parasite"
gg_dir="$software_dir/graph_generator"
cmd_dir="$HOME/nekbone/test/example1"

log_msg "Disabling hyperthreading"
for i in $(cat /sys/devices/system/cpu/cpu*/topology/thread_siblings_list | awk -F',' '{print $2}' | sort -u); do echo 0 > /sys/devices/system/cpu/cpu$i/online; done

log_msg "Enabling msr kernel module"
modprobe msr

cmd="mpiexec --allow-run-as-root -np $(nproc) $cmd_dir/nekbone"

# experiment folder
exp_dir="$HOME/results/$(hostname)/${script_name}_v${version}/$(date -u +'%Y%m%d_%H%M%S_%N')"
mkdir -p "$exp_dir" || exit 1
exp_dir="$(realpath $exp_dir)"

log_msg "Storing system information in %s" "$exp_dir"
$sys_info "$exp_dir" || exit 1

log_msg "Building parasite"
make -C "$parasite_dir" clean
make -C "$parasite_dir" -j $(nproc) || exit 1

log_msg "Building nekbone"
cd "$cmd_dir"
sed -E -i "s/(lp =)([[:digit:]]+)/\1$(nproc)/" SIZE
if [[ ! -f "./nekbone" ]]; then
	./makenek || exit 1
fi

# retrieves packages version
log_msg "Collecting packages information"
packages_reg="lshw|coreutils|dmidecode|util-linux|cpuid|lsb-release|intel-cmt-cat|linux-perf|libnuma-dev|hwloc"
dpkg-query -W > "$exp_dir/packages.txt"
dpkg-query -W | grep -E "($packages_reg).*" > "$exp_dir/main_packages.txt"
# experiment files
# chmod o+w $exp_dir
log_msg "Running experiment"
python3 "$gg_dir/graph_gen.py" --cmd "$cmd" --parasite "$parasite_dir/parasite" --starpart-run "starpart-run" --output-dir "$exp_dir"
# chmod o-w $exp_dir
