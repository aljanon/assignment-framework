#!/bin/bash

########################################
# Critical functions
########################################

log_msg() {
	fmt="$1"
	shift
	script_name="$(basename ${BASH_SOURCE[${#BASH_SOURCE[@]} - 1]})"
	printf "[%s] {%s}: $fmt\n" "$(date --iso-8601=seconds)" "$script_name" "$@"
}

########################################
# Constants
########################################

version=0.3
blas_lib="blas"
repeat=30
script_name="$(basename ${BASH_SOURCE[${#BASH_SOURCE[@]} - 1]})"

########################################
# Script
########################################

software_dir="$(realpath $1)"
shift
[[ ! -d "$software_dir" ]] && log_msg "$software_dir is not a directory. Aborting." >&2 && exit 1
g5k_dir="$software_dir/g5k"
init_script="$g5k_dir/init.g5k"
install_scripts="parasite_install.g5k r_install.g5k"
cblas_dgemm_dir="$software_dir/benchmarks/cblas_dgemm"
parasite_dir="$software_dir/parasite"
parasite_scripts_dir="$software_dir/parasite_scripts"
packages="cpp libopenblas-dev"
# experiment folder
exp_dir="$software_dir/../results/$(hostname)/${script_name}_v${version}/$(date -u +'%Y%m%d_%H%M%S_%N')"
mkdir -p "$exp_dir" || exit 1
exp_dir="$(realpath $exp_dir)"

$init_script "$software_dir" "$exp_dir" || exit 1
for script in $install_scripts; do
	"$g5k_dir/install/$script" || exit 1
done

log_msg "Installing packages"
sudo-g5k apt-get -qq install $packages

log_msg "Building parasite"
make -C "$parasite_dir" clean
make -C "$parasite_dir" -j $(nproc) || exit 1

log_msg "Changing blas library in Makefile"
sed -i "s/\(LIBS.*\) cblas/\1 $blas_lib/" "$cblas_dgemm_dir/Makefile"

log_msg "Building cblas_dgemm"
make -C "$cblas_dgemm_dir" clean
make -C "$cblas_dgemm_dir" -j $(nproc) || exit 1

log_msg "Running experiment"
# retrieves packages version
log_msg "Collecting packages information"
packages_reg="lshw|coreutils|dmidecode|util-linux|cpuid|lsb-release|libopenblas-dev|intel-cmt-cat|linux-perf"
sudo-g5k dpkg-query -W > "$exp_dir/packages.txt"
sudo-g5k dpkg-query -W | grep -E "($packages_reg).*" > "$exp_dir/main_packages.txt"
# experiment files
exp_files=(measurements parameters)
for (( i=0; i<${#exp_files[*]}; i++)); do
	exp_files[i]="$exp_dir/${exp_files[i]}.csv"
done
touch ${exp_files[@]}
chmod o+w ${exp_files[@]}
sudo-g5k "$parasite_scripts_dir/fixed_size_parasite" "$software_dir" "$exp_dir" $@ || exit 1
chmod o-w ${exp_files[@]}

log_msg "Running analysis"
Rscript "$parasite_scripts_dir/fixed_size_parasite_analysis" "$exp_dir"