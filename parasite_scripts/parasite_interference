#!/bin/bash

get_dcache_sizes() {
	for cache in /sys/devices/system/cpu/cpu0/cache/index*; do
		[ "$(cat $cache/type)" != "Instruction" ] && \
			printf "%s " "$(cat $cache/size | sed 's/K/*1024/g' | bc)"
	done
}

get_dcache_line_count() {
	dcache_line_size=$(cat /sys/devices/system/cpu/cpu0/cache/index0/coherency_line_size)
	for cache in $(get_dcache_sizes); do
		printf "%s " "$((cache / dcache_line_size))"
	done
}

repeat=30
access_count=4
p1_cpu_mask="0x1"
p2_cpu_mask="0x4"
dcache_line_size=$(cat /sys/devices/system/cpu/cpu0/cache/index0/coherency_line_size)
result_dir="./results/$(basename $0)/data"
cmd="./software/parasite/parasite"
perf_cmd="perf stat -e cache-misses,cache-references --log-fd 3 --metric-only"

cache_bytes=($(get_dcache_sizes))
cache=($(get_dcache_line_count))
# cache_bytes+=($((${cache_bytes[-1]} * (${cache_bytes[-1]} / ${cache_bytes[-2]}))))
# cache+=($((${cache[-1]} * (${cache[-1]} / ${cache[-2]}))))

points=${1:-$((7 * ${#cache[*]}))}

# point count for each cache
n=$((points / ${#cache[*]}))

printf "P1 cpu mask: %s\n" "$p1_cpu_mask"
printf "P2 cpu mask: %s\n" "$p2_cpu_mask"
printf "Dcache line size: %d\n" "$dcache_line_size"
printf "Cache sizes: %s\n\n" "${cache_bytes[*]}"

printf "Points count: %d\n" $points
printf "Points count for each cache: %d\n" $n
printf "Repeat count: %d\n" $repeat
printf "Access count: %d\n\n" $access_count

filename=""
for (( i=0; i<${#cache_bytes[*]}; i++)); do
	filename+="_L$((i+1))-${cache_bytes[i]}"
done
filename+="_a-${access_count}_r-${repeat}_l-${dcache_line_size}"
filename+="_p1-${p1_cpu_mask}_p2-${p2_cpu_mask}"
filename+=".csv"

result_file="${result_dir}/${filename}"
printf "Result file: %s\n" $result_file

printf "Type,Size(B),Time(ns),Misses(%%)\n" > $result_file

pkill -x parasite

previous=$((${cache[0]} / (n+1)))
for (( i=0; i<${#cache[*]}; i++)); do
	current=${cache[i]}
	inc=$(((current - previous) / n))
	# loop through all possible sizes between $previous and $current caches
	for (( j=previous; j<$current; j=j+inc )); do
		parasite_args="-qqqqq --single -n $j -a $access_count"
		printf "Parasite current args: -n %'d (%'d B) -a %'d\n" $j $((j * dcache_line_size)) $access_count
		for (( k=0; k<repeat; k++ )); do
			result=$(3>&1 taskset $p1_cpu_mask $perf_cmd -x '' $cmd $parasite_args --seed=$k 2>&1 >/dev/null)
			result=$(echo "$result" | sed '1p;$!d' | sed ':a;N;$!ba;s/\n/,/g')
			printf "%d,%d,%s\n" 0 $((j * dcache_line_size)) "$result" >> $result_file
		done
		para_parasite_args="-qqqqq --continuous -n $j"
		printf "Second parasite args %s\n" "$para_parasite_args"
		for (( k=0; k<repeat; k++ )); do
			{ taskset $p2_cpu_mask $cmd $para_parasite_args --seed=$k &>/dev/null & } 2>/dev/null
			sleep 0.2
			result=$(3>&1 taskset $p1_cpu_mask $perf_cmd -x '' $cmd $parasite_args --seed=$k 2>&1 >/dev/null)
			result=$(echo "$result" | sed '1p;$!d' | sed ':a;N;$!ba;s/\n/,/g')
			printf "%d,%d,%s\n" 1 $((j * dcache_line_size)) "$result" >> $result_file
			{ kill %% && wait; } 2>/dev/null
		done
	done
	previous=$current
done
