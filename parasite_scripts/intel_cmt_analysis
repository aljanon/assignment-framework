#!/usr/bin/Rscript

library(readr)
library(ggplot2)
library(stringr)
library(magrittr)
library(tidyr, warn.conflicts = F)
library(dplyr, warn.conflicts = F)
library(purrr, warn.conflicts = F)
args <- commandArgs(TRUE)
result_dir <- args[1]
if (!dir.exists(result_dir)) {
  stop(result_dir, " is not a directory")
}
parameters <- read_csv(paste0(result_dir, "/parameters.csv"),
                       col_types = "cc")

get_parameter <- function(name) {
  return(parameters %>%
            filter(Parameter %in% name) %>%
            pull(Value))
}

cache_sizes <- parameters %>%
    filter(str_detect(Parameter, "L[0-9]+_size")) %>%
    transmute(level = str_match_all(.$Parameter, "L([0-9])+") %>%
                          map_chr(2),
              size = Value) %>%
    mutate_all(as.numeric)

access_count <- get_parameter("access_count")
repeat_count <- as.numeric(get_parameter("repeat"))
dcache_line_size <- as.numeric(get_parameter("dcache_line_size"))
dgemm_size <- as.numeric(get_parameter("dgemm_size"))
p1_cpu_mask <- get_parameter("p1_cpu_mask")
p2_cpu_mask <- get_parameter("p2_cpu_mask")

subt <- sprintf(paste0("Average access count: %s; repeat count: %s; ",
                       "dcache line size: %s\n",
                       "P1 cpu mask: %s; P2 cpu mask: %s\n",
                       "Dgemm size: %s (total: %s B)"),
        format(access_count, big.mark = ","),
        format(repeat_count, big.mark = ","),
        format(dcache_line_size, big.mark = ","), p1_cpu_mask, p2_cpu_mask,
        format(dgemm_size, big.mark = ","),
        format(dgemm_size ** 2 * 8 * 3, big.mark = ","))

cmt_res <- read_csv(paste0(result_dir, "/cmt_measurements.csv"),
                    col_types = "iiii")

cmt_res %<>% 
    group_by(type, Core, size) %>%
    mutate(llc = max_llc * 1024) %>%
    summarise(mean_llc = mean(llc), sd_llc = sd(llc)) %>%
    mutate(minv_llc = mean_llc - sd_llc, maxv_llc = mean_llc + sd_llc) %>%
    select(-sd_llc)

res <- read_csv(paste0(result_dir, "/measurements.csv"),
                col_names = c("type", "size", "time", "miss"),
                col_types = "iidd", skip = 1)

res %<>%
    group_by(type, size) %>%
    summarise(mean_miss = mean(miss), sd_miss = sd(miss)) %>%
    mutate(minv_miss = mean_miss - sd_miss, maxv_miss = mean_miss + sd_miss) %>%
    select(-sd_miss)

breaks <- res %>%
    filter(type == 1) %>%
    slice(seq(1, n(), by = 3)) %>%
    bind_rows(cache_sizes) %>%
    ungroup() %>%
    distinct(size)

labels <- breaks %>%
    left_join(cache_sizes, by = "size") %>%
    transmute(label = if_else(is.na(level),
                              as.character(size),
                              paste0("L", level, "=", size))) %>%
    pull()
breaks %<>% pull()


message(paste0("Saving miss rate results to: ", result_dir, "/misses.csv"))
write_csv(res %>% filter(type == 1), path = paste0(result_dir, "/misses.csv"))
message(paste0("Saving cmt results to: ", result_dir, "/cmt_results.csv"))
write_csv(cmt_res, path = paste0(result_dir, "/cmt_results.csv"))

res %<>% filter(type == 1) %>%
  ungroup() %>%
  union(res %>%
      filter(type == 1) %>%
      ungroup() %>%
      mutate(type = 0,
             mean_miss = res %>% filter(type == 0) %>% pull(mean_miss),
             minv_miss = res %>% filter(type == 0) %>% pull(minv_miss),
             maxv_miss = res %>% filter(type == 0) %>% pull(maxv_miss)))

filename <- paste0(result_dir, "/cache_misses.jpg")
message(paste("Saving cache miss visualization to:", filename))
ggsave(filename, height = 7, width = 14,
  plot = ggplot(res, aes(size, color = factor(type))) +
  geom_point(aes(y = mean_miss, shape = factor(type)), size=4) +
  geom_linerange(aes(ymin = minv_miss, ymax = maxv_miss), alpha = 0.6, size=1) +
  geom_vline(xintercept = cache_sizes$size, alpha = 0.2) +
  scale_y_continuous(name = "Cache miss ratio (%)", expand = c(0, 0),
                     limits = c(0, NA)) +
  scale_x_continuous(name = "Parasite array size (B)",
                     limits = c(breaks[1], breaks[length(breaks)]),
                     breaks = breaks, labels = labels) +
  scale_color_hue("Experiment type",
            breaks=c(0, 1),
            labels=c("without parasite", 'with parasite')) +
  scale_shape_manual("Experiment type",
            breaks=c(0, 1), values=c(1,4),
            labels=c("without parasite", 'with parasite')) +
  theme_minimal() +
  theme(legend.title = element_blank(), legend.position = c(0.1, 0.92), legend.background = element_rect(fill="white", size=0.5, linetype="solid", colour ="black"),text = element_text(size = 24), axis.text.x = element_text(angle = 45, hjust = 1), axis.line = element_line(linetype = "solid"), axis.ticks = element_line(linetype = "solid"), panel.grid = element_line(linetype = "solid")))

filename_cmt <- paste0(result_dir, "/cmt.jpg")
message(paste("Saving cmt visualization to:", filename_cmt))
ggsave(filename_cmt, height = 7, width = 14,
  plot = ggplot(cmt_res %>% filter(type == 1),
                aes(size, mean_llc, color = factor(Core))) +
  geom_line() +
  geom_vline(xintercept = cache_sizes$size, alpha = 0.2) +
  geom_hline(yintercept = cmt_res %>% filter(type == 0) %>% pull(mean_llc),
             alpha = 0.7, color = "red2") +
  geom_hline(yintercept = as.numeric(cmt_res[cmt_res$type == 0,
                                         c("minv_llc", "maxv_llc")]),
             alpha = 0.3, color = "red2") +
  geom_linerange(aes(ymin = minv_llc, ymax = maxv_llc)) +
  scale_y_continuous(name = "Measured cache usage (B)",
                     breaks = breaks, labels = labels,
                     limits = c(breaks[1], breaks[length(breaks)])) +
  scale_x_continuous(name = "Parasite array size (B)",
                      breaks = breaks, labels = labels,
                      limits = c(breaks[1], breaks[length(breaks)])) +
  scale_color_discrete(name = "Software", breaks = c(0, 2),
                       labels = c("Dgemm", "Parasite")) +
  labs(title = "Measured cache usage as a function of the array size.",
       subtitle = subt) +
  theme(axis.text.x = element_text(angle = 45, hjust = 1)))
